
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('chat-message', require('./components/chat/ChatMessage.vue'));
Vue.component('chat-log', require('./components/chat/ChatLog.vue'));
Vue.component('chat-composer', require('./components/chat/ChatComposer.vue'));
Vue.component('wall-record', require('./components/wall/WallRecord.vue'));
Vue.component('wall-log', require('./components/wall/WallLog.vue'));
Vue.component('wall-composer', require('./components/wall/WallComposer.vue'));

const app = new Vue({
    el: '#app',
    data: {
    	messages: [
    		{
				message: 'Привет',
				user: 'Алеша Гаврилов'
			},
			{
				message: 'Здоров) Норм, у тя как?',
				user: 'Сергей Иванов'
			}
			
    	],
        records: [
            {
                record: "С днем рождения",
                user: "Вика Постухова"
            },
            {
                record: "Добра",
                user: "Григорий Анасьев"
            }
        ]
    },
    methods: {
    	addMessage(message) {
            // Add to existing messages
            this.messages.push(message);
        },
        addRecord(record) {
            this.records.push(record);
        }
    }
});
