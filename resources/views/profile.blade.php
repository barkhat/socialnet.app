@extends('layouts.main')

@section("custom_style")
	<link href="{{ asset('css/profile.css') }}" rel="stylesheet">
@endsection

@section('main-content')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Профиль</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<img src="" width="100%" height="100px">
		</div>
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-6">
					<div class="fact-title">Имя:</div>
					<div class="fact-title">Дата рождения:</div>
					<div class="fact-title">Город:</div>
					<div class="fact-title">Семейное положение:</div>
				</div>
				<div class="col-md-6">
					<div class="fact-value">Сергей Иванов</div>
					<div class="fact-value">14 сентября 2000 г.</div>
					<div class="fact-value">Москва</div>
					<div class="fact-value">Холост</div>
				</div>
			</div>
			<div class="row">
				<div class="panel panel-default col-md-12">
					<div class="panel-heading">Стена</div>
					<div class="panel-body wall-list">
						<wall-log :records="records"></wall-log>
						<wall-composer v-on:recordposted="addRecord"></wall-composer>
					</div>
				</div>
			</div>
		</div>
	</div>
		

@endsection