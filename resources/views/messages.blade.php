@extends('layouts.main')

@section("custom_style")
	<link href="{{ asset('css/messages.css') }}" rel="stylesheet">
@endsection

@section('main-content')

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Сообщения</div>
			</div>
		</div>
	</div>
	<div class="chat row">
		<div class="col-md-2">
		<ul class="friends-list nav nav-pills nav-stacked">
			<li><a href="#">Алеша Гаврилов</a></li>
			<li><a href="#">Максим Тоев</a></li>

		</ul>
		</div>
		<div class="col-md-10">
			<chat-log :messages="messages"></chat-log>
			<chat-composer v-on:messagesent="addMessage"></chat-composer>
		</div>	
	</div>

@endsection