@extends('layouts.main')

@section("custom_style")
	<link href="{{ asset('css/friends.css') }}" rel="stylesheet">
@endsection

@section('main-content')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Друзья</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="friends-list col-md-12">
			<div class="friends-item row">
				<a href="#"><img class="friend-photo col-md-2" src="" width="100%" height="100px"></a>
				<div class="col-md-6">
					<a href="#">Алеша Гаврилов</a>
					<div class="clearfix"></div>
					<a href="#">Написать сообщение</a>
				</div>
			</div>
			<div class="friends-item row">
				<a href="#"><img class="friend-photo col-md-2" src="" width="100%" height="100px"></a>
				<div class="col-md-6">
					<a href="#">Максим Тоев</a>
					<div class="clearfix"></div>
					<a href="#">Написать сообщение</a>
				</div>

			</div>
			
		</div>	
	</div>
		

@endsection