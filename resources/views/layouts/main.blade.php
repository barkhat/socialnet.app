@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<ul class="nav nav-pills nav-stacked">
					<li><a href="/profile">Профиль</a></li>
					<li><a href="/friends">Друзья</a></li>
					<li><a href="/messages">Сообщения</a></li>
				</ul>
			</div>
			<div class="col-md-8">
				@yield('main-content')
			</div>
		</div>
	</div>

@endsection